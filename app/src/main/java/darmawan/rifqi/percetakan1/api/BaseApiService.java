package darmawan.rifqi.percetakan1.api;

import darmawan.rifqi.percetakan1.model.Login.CekLogin.Value.CodeCekLogin;
import darmawan.rifqi.percetakan1.model.CreatePemesanan.Value.CodeCreatePemesanan;
import darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Value.CodeDetailList;
import darmawan.rifqi.percetakan1.model.ListPemesanan.List.Value.CodeListPemesanan;
import darmawan.rifqi.percetakan1.model.Login.LoginUser.Value.CodeUserModel;
import darmawan.rifqi.percetakan1.model.Login.CekLogin.Result.ResponseCekLogin;
import darmawan.rifqi.percetakan1.model.CreatePemesanan.Result.ResponseCreatePemesanan;
import darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Result.ResponseDetailList;
import darmawan.rifqi.percetakan1.model.ListPemesanan.List.Result.ResponseListPemesanan;
import darmawan.rifqi.percetakan1.model.Login.LoginUser.Result.ResponseUserModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by yolo on 15/05/18.
 */

public interface BaseApiService {
    @POST("v1")
    Call<ResponseUserModel> loginRequest(@Body CodeUserModel codeUserModel);

    @POST("v1")
    Call<ResponseCreatePemesanan> createPemesanan(@Body CodeCreatePemesanan codeCreatePemesanan);

    @POST("v1")
    Call<ResponseListPemesanan> listPemesanan(@Body CodeListPemesanan codeListPemesanan);

    @POST("v1")
    Call<ResponseDetailList> detailPemesanan(@Body CodeDetailList codeDetailList);

    @POST("v1")
    Call<ResponseCekLogin> cekLogin(@Body CodeCekLogin codeCekLogin);
}
