package darmawan.rifqi.percetakan1.model.CreatePemesanan.Value;

/**
 * Created by yolo on 21/05/18.
 */

public class ADetailPemesanan {
    String produk, oplah, satuan, jenis_order, panjang, lebar, jml_hlm_isi;

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getOplah() {
        return oplah;
    }

    public void setOplah(String oplah) {
        this.oplah = oplah;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public String getJenis_order() {
        return jenis_order;
    }

    public void setJenis_order(String jenis_order) {
        this.jenis_order = jenis_order;
    }

    public String getPanjang() {
        return panjang;
    }

    public void setPanjang(String panjang) {
        this.panjang = panjang;
    }

    public String getLebar() {
        return lebar;
    }

    public void setLebar(String lebar) {
        this.lebar = lebar;
    }

    public String getJml_hlm_isi() {
        return jml_hlm_isi;
    }

    public void setJml_hlm_isi(String jml_hlm_isi) {
        this.jml_hlm_isi = jml_hlm_isi;
    }


}
