package darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Value;

/**
 * Created by yolo on 04/06/18.
 */

public class CodeDetailList {
    String HeaderCode, signature, apiKey;
    ADetailList data;

    public String getHeaderCode() {
        return HeaderCode;
    }

    public void setHeaderCode(String headerCode) {
        HeaderCode = headerCode;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public ADetailList getData() {
        return data;
    }

    public void setData(ADetailList data) {
        this.data = data;
    }
}
