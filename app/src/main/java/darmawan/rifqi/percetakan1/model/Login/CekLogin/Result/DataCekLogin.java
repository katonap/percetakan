package darmawan.rifqi.percetakan1.model.Login.CekLogin.Result;

/**
 * Created by yolo on 04/06/18.
 */

public class DataCekLogin {
    String apiKey, username, nama_lengkap, alamat, no_telp, level, level_text;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLevel_text() {
        return level_text;
    }

    public void setLevel_text(String level_text) {
        this.level_text = level_text;
    }
}
