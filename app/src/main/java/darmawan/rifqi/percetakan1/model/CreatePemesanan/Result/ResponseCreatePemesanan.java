package darmawan.rifqi.percetakan1.model.CreatePemesanan.Result;

/**
 * Created by yolo on 21/05/18.
 */

public class ResponseCreatePemesanan {
    String status, message, errorMessage;
    DataCreatePemesanan data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public DataCreatePemesanan getData() {
        return data;
    }

    public void setData(DataCreatePemesanan data) {
        this.data = data;
    }
}
