package darmawan.rifqi.percetakan1.RecyclerView;

/**
 * Created by fahlepyrizal01 on 06/04/18.
 */

import android.app.ProgressDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import darmawan.rifqi.percetakan1.R;
import darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Result.DataListPemesanan;

public class rvAdapterListOrder extends RecyclerView.Adapter<rvAdapterListOrder.ViewHolder> {
  private List<DataListPemesanan> data;


  /*public rvAdapterListOrder(Context context, OnStartDragListener dragStartListener) {
    mDragStartListener = dragStartListener;
    mItems.addAll(Arrays.asList(context.getResources().getStringArray(R.array.dummy_items)));

  }*/

  public rvAdapterListOrder(List<DataListPemesanan> data) {
    this.data = data;
    this.notifyDataSetChanged();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_order, parent, false);
    ViewHolder itemViewHolder = new ViewHolder(view, parent);
    return itemViewHolder;
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.tvnosoki.setText("Nomor Soki : " + data.get(position).getNo_soki());
    holder.tvnosp.setText("Nomor SP : " + data.get(position).getNo_sp());
    holder.tvnamaorder.setText("Nama Order : " + data.get(position).getNama_order());
    holder.tvnamapemesan.setText("Nama Pemesan : " + data.get(position).getNama_pemesan());
    holder.tvtanggalbuat.setText("Tanggal Order : " + data.get(position).getTgl_order());
    holder.tvalamatpemesan.setText("Alamat Pemesan : " + data.get(position).getAlamat_pemesan());
    holder.tvnomortelponpemesan.setText("Nomor Tepon Pemesan : " + data.get(position).getNo_telp_pemesan());
    holder.tvstatus.setText("Status : " + data.get(position).getStatus());
  }

  @Override
  public int getItemCount() {
    return (data == null) ? 0 : data.size();
  }

  /**
   * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
   * "handle" view that initiates a drag event when touched.
   */
  public static class ViewHolder extends RecyclerView.ViewHolder {

    public TextView tvidorder, tvnosoki, tvnosp, tvidpenerima, tvstatus, tvnamapenerima;
    public TextView tvnamaorder;
    public TextView tvnamapemesan;
    public TextView tvtanggalbuat;
    public TextView tvalamatpemesan;
    public TextView tvnomortelponpemesan;
    public TextView tvketerangan;
    private ProgressDialog progressDialog;

    public ViewHolder(final View itemView, final ViewGroup parent) {
      super(itemView);

      tvnosoki = (TextView) itemView.findViewById(R.id.tv_nosoki);
      tvnosp = (TextView) itemView.findViewById(R.id.tv_nosp);
      tvstatus = (TextView) itemView.findViewById(R.id.tv_status);
      tvnamaorder = (TextView) itemView.findViewById(R.id.tv_namaorder);
      tvnamapemesan = (TextView) itemView.findViewById(R.id.tv_namapemesan);
      tvtanggalbuat = (TextView) itemView.findViewById(R.id.tv_tanggalbuat);
      tvalamatpemesan = (TextView) itemView.findViewById(R.id.tv_alamatpemesan);
      tvnomortelponpemesan = (TextView) itemView.findViewById(R.id.tv_nomortelponpemesan);
    }
  }
}
